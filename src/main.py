import psycopg2

conn=psycopg2.connect(host="postgres", database="my_db", user="my_user", password="my_pass")

with conn:
	with conn.cursor() as cursor:
		cursor.execute("CREATE TABLE myTbl (col1 int, col2 int)")
		cursor.execute("INSERT INTO myTbl VALUES(1, 2);")
		cursor.execute("INSERT INTO myTbl VALUES(3, 4);")
		cursor.execute("INSERT INTO myTbl VALUES(5, 6);")
		cursor.execute("SELECT * FROM myTbl;")
		print(cursor.fetchall())
